require 'ability.rb'
module Inv
class Inventory
  def initialize
    @invent = Hash.new
    ObjectSpace.each_object(Item) {|x| @invent[x.name] = x}
    ObjectSpace.each_object(Equipment) {|x| @invent[x.name] = x}
  end
  
  def list
    @invent.each_key do |x|
      puts x + " " + @invent[x].quantity.to_s
    end
  end
end

class Item
attr_reader :name, :description, :quantity
 def initialize(name, description, quantity, &block)
   @name 		= name
   @description 	= description
   @quantity 	= quantity
   if block_given?
     @effect	= block
     end
   end
   
   def list
     Item.each do |item|
       puts item
     end
   end
 
  def use(target)
    @effect.call(target)
  end

 def add
   @quantity = @quantity + 1
   if @quantity > 99
     @quantity = 99
   end
 end
end

#ITEMs
#item = Item.new(name, description, quantity, &block)
#use a block for the item's effect
PotionS = Item.new("Potion S", "Cure 150 HP", 99) do |target| target.curr_hp += 200 end
PotionM = Item.new("Potion M", "Cure 500 HP", 1) do |target| target.curr_hp += 200 end
PotionL = Item.new("Potion L", "Cure 1000 HP", 1) do |target| target.curr_hp += 200 end
FullPotion = Item.new("Full Potion", "Restore full HP", 1) do |target| target.curr_hp += 200 end
EtherS = Item.new("Ether S", "Cure 50 MP", 1) do |target| target.curr_hp += 200 end



class Equipment
attr_reader :name, :description, :quantity, :ability, :att, :atp, :mat, :df, :dfp, :mdf, :mdfp, :elem
  def initialize(name, description, quantity, ability,att, atp, mat, df, dfp, mdf, mdfp, elem)
    @name 		= name
    @description 	= description
    @quantity	= quantity
    @ability		= ability
    @att 		= att
    @atp 		= atp
    @mat		= mat
    @df 		= df
    @dfp 		= dfp
    @mdf 		= mdf
    @mdfp 		= mdfp
    @elem 		= elem
  end
end

#WEAPONS AND ARMORS
#default elements_attributes for weapons and armors
#for weapons, elements_attributes can be 0 or positive number
#for armors, elements_attributes can be 0 or negative number
default = {
:fire		=> 0,
:ice		=> 0,
:lightning	=> 0,
:earth 	=> 0,
:poison 	=> 0,
:gravity 	=> 0,
:water 	=> 0,
:wind 	=> 0,
:holy 	=> 0
}
#ability is an array of abilities listed in ability.rb, elements_attributes is an hash
#weapon = Equipment.new(name, description, quantity, ability, attack, attack%, magic attack, defense, defense%, magicdefense, magicdefense%, elements_attributes)
None = Equipment.new("", "", 1, nil, 0, 0, 0, 0, 0, 0, 0, default)
#Ryuuji's weapons (aka cloud)
BusterSword 	= Equipment.new("Buster Sword", "Initially Equipped", 	1, [], 18, 96, 2, 0, 0, 0, 0, default)
MythrilSaber 	= Equipment.new("Mythril Saber", "Initially Equipped", 	1, [], 23, 98, 4, 0, 0, 0, 0, default)
Hardedge 		= Equipment.new("Hardedge", "Initially Equipped", 		1, [], 32, 98, 6, 0, 0, 0, 0, default)
ButterflyEdge 	= Equipment.new("Butterfly Edge", "Initially Equipped", 	1, [], 39, 100, 8, 0, 0, 0, 0, default)
EnhanceSword 	= Equipment.new("Enhance Sword", "Initially Equipped", 	1, [], 43, 107, 16, 0, 0, 0, 0, default)
Organics	 	= Equipment.new("Organics", "Initially Equipped", 		1, [], 62, 103, 15, 0, 0, 0, 0, default)
CrystalSword 	= Equipment.new("Crystal Sword", "Initially Equipped", 	1, [], 76, 105, 19, 0, 0, 0, 0, default)
ForceStealer 	= Equipment.new("Force Stealer", "Initially Equipped", 	1, [], 36, 100, 7, 0, 0, 0, 0, default)
RuneBlade		= Equipment.new("Rune Blade", "", 					1, [], 40, 108, 9, 0, 0, 0, 0, default)
Murasame		= Equipment.new("Murasame", "", 					1, [], 51, 100, 12, 0, 0, 0, 0, default)
NailBat		= Equipment.new("Nail Bat", "", 					1, [], 70, 100, 0, 0, 0, 0, 0, default)
Yoshiyuki		= Equipment.new("Yoshiyuki", "", 					1, [], 56, 100, 9, 0, 0, 0, 0, default)
Apocalypse		= Equipment.new("Apocalypse", "", 					1, [], 88, 110, 43, 0, 0, 0, 0, default)
HeavensCloud	= Equipment.new("Heaven's Cloud", "", 				1, [], 93, 100, 31, 0, 0, 0, 0, default)
Ragnarok		= Equipment.new("Ragnarok", "", 					1, [], 97, 105, 43, 0, 0, 0, 0, default)
UltimaWeapon	= Equipment.new("Ultima Weapon", "", 				1, [], 100, 110, 51, 0, 0, 0, 0, default)
#Taiga's weapons (aka tifa)
LeatherGlove = Equipment.new("", "", 1, [], 18, 96, 2, 0, 0, 0, 0, default)
MetalKnuckle = Equipment.new("", "", 1, [], 18, 96, 2, 0, 0, 0, 0, default)
MythrilClaw = Equipment.new("", "", 1, [], 18, 96, 2, 0, 0, 0, 0, default)
GrandGlove = Equipment.new("", "", 1, [], 18, 96, 2, 0, 0, 0, 0, default)
TigerFang = Equipment.new("", "", 1, [], 18, 96, 2, 0, 0, 0, 0, default)
DiamondKnuckle = Equipment.new("", "", 1, [], 18, 96, 2, 0, 0, 0, 0, default)
DragonClaw = Equipment.new("", "", 1, [], 18, 96, 2, 0, 0, 0, 0, default)
CrystalGlove = Equipment.new("", "", 1, [], 18, 96, 2, 0, 0, 0, 0, default)
MotorDrive = Equipment.new("", "", 1, [], 18, 96, 2, 0, 0, 0, 0, default)
PlatinumFist = Equipment.new("", "", 1, [], 18, 96, 2, 0, 0, 0, 0, default)
KaiserKnuckle = Equipment.new("", "", 1, [], 18, 96, 2, 0, 0, 0, 0, default)
WorkGlove = Equipment.new("", "", 1, [], 18, 96, 2, 0, 0, 0, 0, default)
PowerSoul = Equipment.new("", "", 1, [], 18, 96, 2, 0, 0, 0, 0, default)
MasterFist = Equipment.new("", "", 1, [], 18, 96, 2, 0, 0, 0, 0, default)
GodsHand = Equipment.new("", "", 1, [], 18, 96, 2, 0, 0, 0, 0, default)
PremiumHeart = Equipment.new("", "", 1, [], 18, 96, 2, 0, 0, 0, 0, default)
#Ami's weapons (aka aerith)

#Minori's weapons (aka yuffie)

#Kitamura's weapons (aka barret)

#


#armor = Equipment.new(name, description, quantity, ability, attack, attack%, defense, defense%, magicdefense, magicdefense%, elements_attributes)
BronzeBangle = Equipment.new("Bronze Bangle", "Initially Equipped", 1, nil, 0, 0, 0, 8, 0, 0, 0, default)

Invento = Inventory.new
end