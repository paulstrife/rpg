module Skill
class Ability
attr_reader :name, :description, :power, :ap, :mp
 def initialize(name, description, type, power, patp, matp, ap, mp, &block)
   @name 		= name
   @description 	= description
   @power		= power
   @type		= type
   @patp		= patp
   @matp		= patp
   @ap		= ap
   @mp 		= mp
   if block_given?
     @effect 	= block
 end
end

  def use(player, target)
    if player.curr_mp >= @mp
    target.curr_hp = target.curr_hp - dealt_damage(player, target)
    player.curr_mp = player.curr_mp - @mp
    player.check_hp
    player.check_mp
    else
      puts "Not enough MPs"
    end
  end
  
  def hit_chance(player, target)
    if player.class == Enemies::Enemy
      ((player.dex / 4) + @patp) + player.dfp - target.dfp      
      else
	((player.dex / 4) + player.atp) + player.dfp - target.dfp
      end
  end
  
  def hitormiss?(player, target)
    random = ((rand(65535) + 1) * 99 / 65535) + 1
    if random < hit_chance(player, target)
      physical_damage(player, target) 
      else
	puts "Miss"
	0
      end
    end
  
  def physical_base_dmg(player)
   player.att + ((player.att + player.level) / 32) * ((player.att * player.level)/32) 
  end
 
  def physical_damage(player, target)
   (@power * (512 - target.df) * physical_base_dmg(player)) / (16*512)
  end

  def magical_base_dmg(player)
    6 * (player.mat + player.level)
  end
  
  def magical_damage(player, target)
    (@power * (512 - target.mdf) * magical_base_dmg(player)) / (16*512)
  end
  
  def cures(player)
    magical_base_dmg(player) + (22 * @power)
    end
  
  def dealt_damage(player, target)
    case @type
    when "physical"	then hitormiss?(player, target) 
    when "magic"	then magical_damage(player, target)
    when "curative"   then cures(player)
  end
end
end


#ABILITY
#ability = Ability.new(name, description, type, power, ap, mp, &block)
#ability's type can be "physical" or "magic", ap are the ability points needed to learn the ability
#mp needed to use the ability and block for effects caused by the ability if any
Attack = Ability.new("Attack", "Attack with equipped weapon", "physical", 16, 0, 0, 0, 0)
Fire = Ability.new("Fire", "Fire", "magic", 10, 0, 0, 20, 10)
Ice = Ability.new("Ice", "Fire", "magic", 10, 0, 0, 20, 10)


#ENEMY ABILITY
MachineGun = Ability.new("Machine Gun", "Shoot with Machine Gun", "physical", 6, 100, 0, 0, 0)
Rifle = Ability.new("Rifle", "Shoots with rifle", "physical", 41, 100, 0, 0, 0)

end