class Player
attr_reader :name, :level, :exp, :max_hp, :max_mp, :str, :dex, :spr, :vit, :mag, :lck, :att, :atp, :df, :dfp, :mat, :mdf, :weapon, :c_timer, :v_timer, :t_timer
attr_accessor :curr_hp, :curr_mp, :ability_flags, :ability_ap, :elems_attack, :status, :timer, :ready
  def initialize(name, level, weapon, armor, accessory, game)
	@game = game
    @name	= name
    @level  	= level
    @exp  	= exp_for_lvl(@level - 1)
	default_elems_attack
	default_elems_defense
    @ability_flags = Hash.new
    @ability_ap = Hash.new
    equip_weapon 	(weapon)
    equip_armor 	    	(armor)
    equip_accessory	(accessory)
    stats_ranks
    base_stats
    hp_mp_calc
    stats
    default_status
  end
  
  def battle_timers_set
	  @ready = false
	  @c_timer = 0
	  @v_timer = 0
	  @t_timer = 0
	  @c_timer_inc = 136
	  @v_timer_inc = 182
	  @t_timer_inc = 182
	  @c_timer_units = 0
	  @v_timer_units = 0
  end
  
  def c_timer_increase
	  @c_timer += @c_timer_inc
	  if @c_timer >= 1024
		  @c_timer_units += 1
		  @c_timer = @c_timer - 1024
	  end
  end

  def v_timer_increase
	  @v_timer += @v_timer_inc
	  if @v_timer >= 1024
		  @v_timer_units += 1
		  @v_timer = @v_timer - 1024
	  end
  end  
  
  def t_timer_increase
	  @t_timer += @t_timer_inc
	  if @t_timer >= 8192
		  @ready = true
		  #~ $turn.signal
	  end
  end
  
  def t_timer_halt
	  @t_timer = @t_timer
  end
end

class Battle
	attr_reader :all
	def initialize(allies, opponents)
		@allies = allies
		@opponents = opponents
		@all = @allies + @opponents
		@listall = Hash.new
		@totexp = 0
		@totgil 	= 0
		@totap = 0
		@escape = false
		@battle_time = 0
		@queue = []
	end
	
	def battle_timer_start
	set_battle_timers
	Thread.new do
		while true
			sleep(0.065)
			@battle_time += 0.065
			timer_increase
			end
		end
	end
	
	def set_battle_timers
		@all.each do |char|
			char.battle_timers_set
		end
	end
	
	def timer_increase
		@all.each do |char|
			char.c_timer_increase
			char.v_timer_increase
			if char.ready
				char.t_timer_halt
				queue_push(char)
				else
				char.t_timer_increase
			end
		end
	end
	
	def turn
		if @queue[0].nil? == false
			@curr_player = @queue[0]
			@queue[0] = nil
			@queue.compact!
			puts @curr_player.name
			ally_or_enemy
		else puts "Wait"
		end
	end
	

	def queue_push(char)
		if @queue.include?(char) == false
			@queue.push(char)
		end
	end
end

  
  