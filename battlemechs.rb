class Battle
	attr_reader :all
	def initialize(allies, opponents)
		@allies = allies
		@opponents = opponents
		@all = @allies + @opponents
		@listall = Hash.new
		@totexp = 0
		@totgil 	= 0
		@totap = 0
		@escape = false
		@battle_time = 0
		@battle_speed = 128
	end
	
	def battle_timer_start
	Thread.new do
		while true
			@battle_time += 0.065
			sleep(0.065)
			end
		end
	end
	
	def speed_value
		@speed_value = (32768/(120 + ((@battle_speed * 15) / 8 )))
	end
	
	def normal_speed
		party_avg_dex
		@normal_speed = ((@party_avg_dex) + 50)
	end
	
	def party_avg_dex
		tot_dex = 0
		@allies.each do |char|
			tot_dex += char.dex
		end
		@party_avg_dex = (tot_dex / @allies.length)
	end
	
	def timer_increase
		@all.each do |char|
			char.c_timer_increase
			char.v_timer_increase
			char.t_timer_increase
		end
	end

  
	def timer
		@all.each do |char|
		battle_end?
		break if @end
		time = Thread.new {
		$mutex.synchronize {
		@curr_player = char
		ally_or_enemy
		$cv.wait($mutex)
		}
		}
		time
	char.timer.join
    end
  end
  
  def ally_or_enemy
    if @curr_player.class == Player
      show_command
      elsif @curr_player.class == Enemies::Enemy
	@curr_player.behaviour(@allies[0])
	else nil
	end
    end
  

  def list
    @all.each do |char|
      if char.status[:death]
	puts "is Death"
	else @listall[char.name] = char
	end
    end
  end

  
  def show
    @allies.party.each do |char|
      puts char.name
    end
    @opponents.party.each do |char|
      puts char.name
    end
  end
  
  def show_command
    puts "Attack"
    puts "Magic"
    puts "Item"
    puts "Run"
    @command = gets.chomp
    case @command
      when "Attack" then attack
      when "Magic" then puts "Choose enemy"
      when "Item" then Inv::Invento.list
      when "Run" then @escape = true
    end
  end
  
  def choose_enemy
    list
    puts @listall.keys
    @temp = gets.chomp
    @target = @listall[@temp]
  end
  
  def attack
    choose_enemy
    Skill::Attack.use(@curr_player, @target)
    @target.check_hp
	dead?
    puts @target.curr_hp
  end
  
  def dead?
	if @target.class == Enemies::Enemy
	  if @target.status[:death]
	  @totexp += @target.exp
	  @totap 	+= @target.ap
	  @totgil		+= @target.gil
	  end
	end
  end
	  
  
  def defeat?(group)
    count = 0
    group.each do |char|
      if char.status[:death]
	count += 1
      end
    end
    if group.length == count
      true
	else
	  false
      end
    end
    
  def battle_end?
	@end = false
    if defeat?(@allies)
      puts "Game Over"
	  @end = true
      elsif defeat?(@opponents)
		puts "Win"
		@end = true
		elsif @escape
		  @end = true
	  else @end
      end
    end
	
	def ally_gain
	  @gainedxp = @totexp/3
	  @gainedap = @totap/3
	  @all.each do |char|
		char.exp += @gainedxp
	  end
	end
	
    def battle_start
	  until @end
	  timer
	end
	puts "Battle end"
  end
end