class Game
	attr_reader :seconds, :minutes, :hours
	def initialize
		@global_timer = 0
		@seconds = 0
		@minutes = 0
		@hours = 0
		@total_gil = 0
		global_timer_start
	end
	
	def global_timer_start
		Thread.new do
		while true
			sleep(1)
			@global_timer += 1
			update_time
			end
		end
	end
	
	def update_time
		@seconds += 1
		if @seconds == 60
			@minutes += 1
			@seconds = 0
		end
		if @minutes == 60
			@hours += 1
			@minutes = 0
		end
	end
end

g = Game.new

loop do
	sleep(rand(40))
	puts g.seconds
	puts g.minutes
	puts g.hours
end
