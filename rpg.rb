require 'thread'
$mutex = Mutex.new
$cv = ConditionVariable.new
class Game
	attr_reader :seconds, :minutes, :hours
	def initialize
		@global_timer = 0
		@seconds = 0
		@minutes = 0
		@hours = 0
		@total_gil = 0
		global_timer_start
	end
	
	def global_timer_start
		Thread.new do
		while true
			sleep(1)
			@global_timer += 1
			update_time
			end
		end
	end
	
	def update_time
		@seconds += 1
		if @seconds == 60
			@minutes += 1
			@seconds = 0
		end
		if @minutes == 60
			@hours += 1
			@minutes = 0
		end
	end
end

class Player
attr_reader :name, :level, :exp, :max_hp, :max_mp, :str, :dex, :spr, :vit, :mag, :lck, :att, :atp, :df, :dfp, :mat, :mdf, :weapon, :c_timer, :v_timer, :t_timer
attr_accessor :curr_hp, :curr_mp, :ability_flags, :ability_ap, :elems_attack, :status, :timer, :ready
  def initialize(name, level, weapon, armor, accessory)
    @name	= name
    @level  	= level
    @exp  	= exp_for_lvl(@level - 1)
	default_elems_attack
	default_elems_defense
    @ability_flags = Hash.new
    @ability_ap = Hash.new
    equip_weapon 	(weapon)
    equip_armor 	    	(armor)
    equip_accessory	(accessory)
    stats_ranks
    base_stats
    hp_mp_calc
    stats
    default_status
  end
  
  def battle_timers_set
	  @ready = false
	  @c_timer = 0
	  @v_timer = 0
	  @t_timer = 0
	  @c_timer_inc = 136
	  @v_timer_inc = battle.battle_speed
	  @t_timer_inc = (((@dex + 50) * @v_timer_inc)/battle.battle_speed)
	  @c_timer_units = 0
	  @v_timer_units = 0
  end
  
  def c_timer_increase
	  @c_timer += @c_timer_inc
	  if @c_timer >= 1024
		  @c_timer_units += 1
		  @c_timer = @c_timer - 1024
	  end
  end
  
  def v_timer_increase
	  @v_timer += @v_timer_inc
	  if @v_timer >= 1024
		  @v_timer_units += 1
		  @v_timer = @v_timer - 1024
	  end
  end  
  
  def t_timer_increase
	  @t_timer += @t_timer_inc
	  if @t_timer >= 8192
		  @ready = true
		  @t_timer = 0
	  end
  end
  
  def timer
	@timer = Thread.new {$mutex.synchronize{(sleep((100 - rand(20))/@dex)); $cv.signal}}
  end
    
  def hp_mp_calc
    @max_hp = 230
    @max_mp = 15
    real_level = @level
    @level = 2
    (real_level - @level).times do lvl_up
    end
  end
  
  def check_hp
    if @curr_hp <= 0
	  @curr_hp = 0
      @status[:death] = true
	  puts "#{@name} is death"
    elsif @max_hp > 9999
      @max_hp = 9999
    elsif @curr_hp > @max_hp
      @curr_hp = @max_hp
    end
  end
  
  def check_mp
    if @curr_mp <= 0
      @curr_mp = 0
    elsif @max_mp > 999
      @max_mp = 999
    elsif @curr_mp > @max_mp
      @curr_mp = @max_mp
    end
  end
  
  def base_stats
    @str 	= @base_str	= 11
    @dex     = @base_dex	= 1
    @vit 	= @base_vit	= 5
    @mag 	= @base_mag	= 8
    @spr 	= @base_spr	= 5
    @lck 	= 14
  end
      
  def stats
    @lck	= 14
    @att	= @str + @weapon.att
    @atp	= @weapon.atp
    @df 	= @vit + @armor.df
    @dfp    	= (@dex / 4) + @armor.dfp
    @mat   	= @mag + @weapon.mat
    @mdf   	= @spr + @armor.mdf
    @mdfp 	= @armor.mdfp
  end

  def stats_ranks
	@stats_ranks = {
	:str 	=> 1,
	:vit 	=> 6,
	:mag	=> 3,
	:spr 	=> 4,
	:dex 	=> 26
	}
  end

  def default_status
	@status 	= {
	:sleep		=> false,
	:poison		=> false,
	:slow			=> false,
	:haste		=> false,
	:stop			=> false,
	:silence		=> false,
	:confuse	=> false,
	:berserk		=> false,
	:petrify		=> false,
	:paralyze	=> false,
	:barrier		=> false,
	:mbarrier	=> false,
	:regen		=> false,
	:death		=> false
	}
  end

  def default_elems_attack
    @elems_attack	= {
	:fire			=> 0,
	:ice			=> 0,
	:lightning 	=> 0,
	:earth 		=> 0,
	:poison 		=> 0,
	:gravity 		=> 0,
	:water 		=> 0,
	:wind 		=> 0,
	:holy 		=> 0
	}
  end
  
  def default_elems_defense
    @elems_defense	= {
	:fire			=> 0,
	:ice			=> 0,
	:lightning 	=> 0,
	:earth 		=> 0,
	:poison 		=> 0,
	:gravity 		=> 0,
	:water 		=> 0,
	:wind 		=> 0,
	:holy 		=> 0
	}
  end
      
  def exp_gain(gain)
	if @status[:death]
	  gain = 0
	  puts "#{@name} is death"
	  elsif @level == 99
		@exp = @exp
		else
		  @exp = @exp + gain
		end
		if  @exp > exp_for_lvl(level)
		  lvl_up
		  check_lvl
		  end
  end
  
  def exp_for_lvl(level)
    mod = 76
	exp_need = 0
	0.upto(level) do |count|
	  exp_need = exp_need + (mod * ((count + 1)**2) / 10).to_i
	  end
	return exp_need
  end
  
  def lvl_up
    @level 		= @level 	+ 1
    @curr_hp 	= @max_hp	= hp_up
    check_hp
    @curr_mp 	= @max_mp	= mp_up
    check_mp
    @lck	= @lck 	+ 1
    @str	= stats_up(stats_ranks[:str], @str)
    @dex	= stats_up(stats_ranks[:dex], @dex)
    @vit	= stats_up(stats_ranks[:vit], @vit)
    @mag	= stats_up(stats_ranks[:mag], @mag)
    @spr	= stats_up(stats_ranks[:spr], @spr)
  end
    
  def check_lvl
    lvl = @level
    exp_gain(0)
    if @level > lvl
      check_lvl
    end
  end
  
  def hp_gradient
    case @level
      when 2..11 then 19
      when 12..21 then 42
      when 22..31 then 72
      when 32..41 then 100
      when 42..51 then 121
      when 52..61 then 137
      when 62..81 then 120
      when 82..99 then 98
    end
  end
    
  def hp_base
    case @level
      when 2..11 then 200
      when 12..21 then -40
      when 22..31 then -640
      when 32..41 then -1440
      when 42..51 then -2280
      when 52..61 then -3080
      when 62..81 then -2040
      when 82..99 then -200
    end
  end
  
    def hp_baseline
      (hp_base + (@level - 1) * hp_gradient)
    end
    
    def hp_diff
      (rand(8) + 1 + ((100 * hp_baseline)/@max_hp) - 100).abs
    end
    
    def hp_gain
      case hp_diff
	when 0 then 40
	when 1 then 50
	when 2 then 50
	when 3 then 60
	when 4 then 70
	when 5 then 80
	when 6 then 90
	when 7 then 100
	when 8 then 110
	when 9 then 120
	when 10 then 130
	when 11..100 then 150
      end
    end
    
    def hp_up
      @max_hp = (@max_hp + (hp_gradient/100.0) * hp_gain).to_i
    end
    
   def mp_gradient
    case @level
      when 2..11 then 64
      when 12..21 then 78
      when 22..31 then 90
      when 32..41 then 101
      when 42..51 then 112
      when 52..61 then 112
      when 62..81 then 96
      when 82..99 then 73
    end
  end
    
  def mp_base
    case @level
      when 2..11 then 12
      when 12..21 then 0
      when 22..31 then -26
      when 32..41 then -58
      when 42..51 then -102
      when 52..61 then -102
      when 62..81 then -4
      when 82..99 then 180
    end
  end
    
    def mp_baseline
      mp_base + (((@level - 1) * mp_gradient) / 10)
    end
    
    def mp_diff
      (rand(8) + 1 + ((100 * mp_baseline)/@max_mp) - 100).abs
    end
    
    def mp_gain
      case mp_diff
	when 0 then 20
	when 1 then 30
	when 2 then 30
	when 3 then 50
	when 4 then 70
	when 5 then 80
	when 6 then 90
	when 7 then 100
	when 8 then 110
	when 9 then 120
	when 10 then 140
	when 11..100 then 160
      end
    end
    
    def mp_up
      @max_mp = (@max_mp + (mp_gradient/1000.0) * mp_gain).to_i
    end
    
  def stats_gradients(rank)
    grad2_11 	= [130, 120, 130, 130, 120, 120, 110, 120, 100, 110, 100, 110, 100, 110, 110, 115, 114, 112, 100, 100, 100, 100, 100, 100, 95, 80, 72, 70, 70, 65]
    grad12_21 	= [160, 130, 140, 120, 128, 125, 130, 135, 130, 120, 115, 120, 122, 122, 105, 127, 118, 115, 108, 111, 108, 110, 102, 100, 90, 85, 69, 53, 70, 63]
    grad22_31 	= [160, 133, 140, 135, 130, 117, 145, 130, 125, 122, 124, 115, 140, 130, 104, 121, 114, 111, 115, 112, 114, 127, 101, 107, 88, 115, 76, 63, 70, 76]
    grad32_41 	= [120, 135, 110, 120, 130, 118, 110, 110, 120, 123, 118, 102, 135, 98, 102, 108, 95, 103, 108, 97, 106, 77, 88, 85, 85, 92, 77, 70, 71, 61]
    grad42_51 	= [70, 120, 90, 85, 77, 93, 100, 85, 77, 80, 107, 91, 83, 83, 93, 86, 82, 83, 83, 53, 63, 50, 70, 77, 62, 78, 68, 69, 67, 49]
    grad52_61 	= [60, 72, 70, 70, 72, 52, 75, 70, 67, 75, 78, 37, 40, 45, 87, 68, 71, 48, 55, 45, 45, 41, 57, 60, 52, 64, 50, 58, 48, 36]
    grad62_81 	= [50, 55, 48, 53, 61, 44, 44, 60, 43, 55, 42, 40, 30, 44, 51, 41, 37, 39, 31, 39, 33, 40, 45, 30, 39, 27, 22, 28, 16, 28]
    grad82_99 	= [30, 21, 27, 32, 35, 35, 31, 35, 31, 44, 36, 40, 25, 33, 25, 24, 30, 25, 24, 34, 26, 31, 24, 24, 18, 21, 21, 20, 16, 20]
    case @level
      when 2..11 then grad2_11[rank]
      when 12..21 then grad12_21[rank]
      when 22..31 then grad22_31[rank]
      when 32..41 then grad32_41[rank]
      when 42..51 then grad42_51[rank]
      when 52..61 then grad52_61[rank]
      when 62..81 then grad62_81[rank]
      when 82..99 then grad82_99[rank]
    end
  end
  
  def stats_bases(rank)
    bsln2_11   	= [12, 13, 12, 12, 10, 12, 10, 11, 12, 9, 9, 11, 9, 10, 8, 9, 10, 10, 10, 9, 10, 8, 9, 9, 8, 7, 6, 6, 5, 5]
    bsln12_21 	= [9, 12, 10, 13, 9, 12, 8, 10, 9, 8, 9, 10, 9, 9, 9, 9, 9, 10, 10, 8, 9, 7, 9, 9, 9, 7, 7, 9, 6, 6]
    bsln22_31 	= [9, 11, 11, 11, 8, 14, 5, 11, 10, 8, 7, 12, 6, 7, 11, 11, 10, 10, 9, 9, 8, 4, 10, 8, 12, 1, 6, 8, 7, 4]
    bsln32_41 	= [21, 11, 21, 15, 8, 14, 17, 16, 11, 8, 8, 17, 8, 16, 13, 15, 16, 13, 11, 17, 11, 20, 15, 14, 13, 8, 6, 6, 7, 9]
    bsln42_51 	= [44, 17, 32, 33, 30, 23, 17, 27, 29, 26, 11, 21, 29, 22, 16, 23, 22, 21, 21, 32, 29, 31, 21, 18, 22, 13, 10, 7, 9, 14]
    bsln52_61 	= [50, 43, 43, 40, 33, 49, 30, 33, 34, 29, 26, 49, 51, 43, 18, 32, 28, 39, 35, 37, 39, 36, 28, 25, 29, 20, 19, 13, 18, 20]
    bsln62_81 	= [57, 53, 56, 51, 40, 55, 50, 37, 49, 42, 48, 48, 57, 45, 40, 48, 49, 45, 51, 42, 47, 37, 35, 44, 38, 42, 36, 31, 38, 24]
    bsln82_99 	= [73, 80, 73, 69, 61, 62, 61, 58, 58, 48, 53, 48, 62, 54, 60, 62, 55, 57, 57, 47, 53, 46, 53, 50, 55, 46, 37, 37, 38, 30]
    case @level
      when 2..11 then bsln2_11[rank]
      when 12..21 then bsln12_21[rank]
      when 22..31 then bsln22_31[rank]
      when 32..41 then bsln32_41[rank]
      when 42..51 then bsln42_51[rank]
      when 52..61 then bsln52_61[rank]
      when 62..81 then bsln62_81[rank]
      when 82..99 then bsln82_99[rank]
    end
  end
  
  def stats_baseline(rank)
    stats_bases(rank) + (stats_gradients(rank) * @level / 100)
  end
  
  def stats_diff(stats_rank, stat)
    rand(8) + 1 + stats_baseline(stats_rank).to_i - stat
  end
  
  def stats_gain(stats_rank, stat)
    case stats_diff(stats_rank, stat).abs
      when 0..3 then 0
      when 4..6 then 1
      when 7..9 then 2
      when 10..100 then 3
    end
  end
  
  def stats_up(stats_rank, stat)
    stat = stat + stats_gain(stats_rank, stat)
  end
  
  def equip_weapon(weapon)
	if @weapon != nil
	  ability_remove(@weapon)
	end
    @weapon = weapon
	@elems_attack.merge!(weapon.elem)
	if @weapon.ability != nil
	  ability_add(@weapon)
	end
  end
  
  def equip_armor(armor)
	if @armor != nil
	  ability_remove(@armor)
	end
	@armor = armor
	@elems_defense.merge!(armor.elem)
	if @armor.ability != nil
	  ability_add(@armor)
	end
  end
  
  def equip_accessory(accessory)
    @accessory = accessory
  end
  
  def ability_remove(equipment)
	if equipment.ability != nil
	  (equipment.ability).each do |ability|
		ability_false(ability)
	  end
	end
  end
  
  def ability_add(equipment)
	if equipment.ability != nil
	  (equipment.ability).each do |ability|
		ability_true(ability)
	  end
	end
  end
  
  def ability_true(ability)
    @ability_flags[ability.name] = true
	if @ability_ap.has_key?(ability.name)
    @ability_ap[ability.name] = @ability_ap[ability.name]
	else
	  @ability_ap[ability.name] = 0
	end
  end
  
  def ability_false(ability)
	if not_learnt(ability)
	  @ability_flags[ability.name] = false
	end
  end
  
  def not_learnt(ability)
	if @ability_ap[ability.name] >= ability.ap
	  false
	  else
		true
	  end
	end
  
  def ap_gain(ability, gain)
	if @ability_flags[ability.name]
	  @ability_ap[ability.name] = @ability_ap[ability.name] + gain
	  if @ability_ap[ability.name] >= ability.ap
		@ability_ap[ability.name] = ability.ap
	  end
	end
  end
  
  def learning(ability)
    if ability_ap[ability.name] >= ability.ap
      ability_flags[ability.name] = true
    end
  end
end

class Party
  attr_reader :party
  def initialize(*member)
    @temp = [member]
    @party = Array.new
    @temp[0].each do |member|
      @party.push member
    end
  end
end

class Battle
	attr_reader :all, :battle_speed
	def initialize(allies, opponents)
		@allies = allies
		@opponents = opponents
		@all = @allies + @opponents
		@listall = Hash.new
		@totexp = 0
		@totgil 	= 0
		@totap = 0
		@escape = false
		@battle_time = 0
		@battle_speed = 128
		@queue = []
	end
	
	def battle_timer_start
	Thread.new do
		while true
			@battle_time += 0.065
			sleep(0.065)
			end
		end
	end
	
	def speed_value
		@speed_value = (32768/(120 + ((@battle_speed * 15) / 8 )))
	end
	
	def normal_speed
		party_avg_dex
		@normal_speed = ((@party_avg_dex) + 50)
	end
	
	def party_avg_dex
		tot_dex = 0
		@allies.each do |char|
			tot_dex += char.dex
		end
		@party_avg_dex = (tot_dex / @allies.length)
	end
	
	def timer_increase
		@all.each do |char|
			char.c_timer_increase
			char.v_timer_increase
			char.t_timer_increase
		end
	end

  
	def timer
		@all.each do |char|
		battle_end?
		break if @end
		time = Thread.new {
		$mutex.synchronize {
		@curr_player = char
		ally_or_enemy
		$cv.wait($mutex)
		}
		}
		time
	char.timer.join
    end
  end
  
  def ally_or_enemy
    if @curr_player.class == Player
      show_command
      elsif @curr_player.class == Enemies::Enemy
	@curr_player.behaviour(@allies[0])
	else nil
	end
    end
  

  def list
    @all.each do |char|
      if char.status[:death]
	puts "is Death"
	else @listall[char.name] = char
	end
    end
  end

  
  def show
    @allies.party.each do |char|
      puts char.name
    end
    @opponents.party.each do |char|
      puts char.name
    end
  end
  
  def show_command
    puts "Attack"
    puts "Magic"
    puts "Item"
    puts "Run"
    @command = gets.chomp
    case @command
      when "Attack" then attack
      when "Magic" then puts "Choose enemy"
      when "Item" then Inv::Invento.list
      when "Run" then @escape = true
    end
  end
  
  def choose_enemy
    list
    puts @listall.keys
    @temp = gets.chomp
    @target = @listall[@temp]
  end
  
  def attack
    choose_enemy
    Skill::Attack.use(@curr_player, @target)
    @target.check_hp
	dead?
    puts @target.curr_hp
  end
  
  def dead?
	if @target.class == Enemies::Enemy
	  if @target.status[:death]
	  @totexp += @target.exp
	  @totap 	+= @target.ap
	  @totgil		+= @target.gil
	  end
	end
  end
	  
  
  def defeat?(group)
    count = 0
    group.each do |char|
      if char.status[:death]
	count += 1
      end
    end
    if group.length == count
      true
	else
	  false
      end
    end
    
  def battle_end?
	@end = false
    if defeat?(@allies)
      puts "Game Over"
	  @end = true
      elsif defeat?(@opponents)
		puts "Win"
		@end = true
		elsif @escape
		  @end = true
	  else @end
      end
    end
	
	def ally_gain
	  @gainedxp = @totexp/3
	  @gainedap = @totap/3
	  @all.each do |char|
		char.exp += @gainedxp
	  end
	end
	
    def battle_start
	  until @end
	  timer
	end
	puts "Battle end"
  end
end

require 'ability.rb'
require 'inventory.rb'
require 'enemy.rb'
#----------------------------------------------------------------------------------#
#----------------------------------------------------------------------------------#
cloud = Player.new("Cloud", 7, Inv::BusterSword, Inv::None, Inv::None)
barret = Player.new("Barret", 7, Inv::BusterSword, Inv::BronzeBangle, Inv::None)
tifa = Player.new("Tifa", 7, Inv::BusterSword, Inv::None, Inv::None)

P1 = Party.new(cloud, barret, tifa)
P2 = Party.new(Enemies::Grunt)

battle = Battle.new(P1.party, P2.party)

battle.battle_start














