require 'ability.rb'

module Enemies
class Enemy
attr_reader :name, :level, :hp, :mp, :exp, :ap, :gil, :win, :att, :mat, :df, :mdf, :dfp, :dex, :lck, :timer
attr_accessor :status, :curr_hp, :curr_mp
  def initialize(name, level, hp, mp, exp, ap, gil, win, att, mat, df, mdf, dfp, dex, lck, &block)
    @name 		= name
    @level 		= level
    @hp 			= hp
    @curr_hp	= hp
    @mp			= mp
    @curr_mp	= mp
	@exp			= exp
    @ap			= ap
    @gil			= gil
    @win			= win
    @att 		= att
    @mat 		= mat
    @df 			= df
    @mdf			= mdf
    @dfp			= dfp
    @dex			= dex
    @lck			= lck
	if block_given?
	  @ai = block
	end
    default_status
	default_elems_attack
	default_elems_defense
end

  def battle_timers_set(normal_speed)
	  @ready = false
	  @c_timer = 0
	  @v_timer = 0
	  @t_timer = 0
	  @c_timer_inc = 136
	  @v_timer_inc = 2 * $battle_speed
	  @t_timer_inc = ((@dex * @v_timer_inc)/normal_speed)
	  @c_timer_units = 0
	  @v_timer_units = 0
  end
  
  def c_timer_increase
	  @c_timer += @c_timer_inc
	  if @c_timer >= 1024
		  @c_timer_units += 1
		  @c_timer = @c_timer - 1024
	  end
  end
  
  def v_timer_increase
	  @v_timer += @v_timer_inc
	  if @v_timer >= 1024
		  @v_timer_units += 1
		  @v_timer = @v_timer - 1024
	  end
  end  
  
  def t_timer_increase
	  @t_timer += @t_timer_inc
	  if @t_timer >= 8192
		  @ready = true
		  @t_timer = 0
	  end
  end

      
  def timer
	@timer = Thread.new {$mutex.synchronize{(sleep((100 - rand(20))/@dex)); $cv.signal}}
  end
  
  def default_status
  @status 	= {
  :sleep		=> false,
  :poison	=> false,
  :slow		=> false,
  :haste		=> false,
  :silence	=> false,
  :confuse	=> false,
  :berserk	=> false,
  :petrify	=> false,
  :paralyze	=> false,
  :death		=> false
  }
  end

  def default_elems_attack
    @elems_attack	= {
	:fire			=> 0,
	:ice			=> 0,
	:lightning 	=> 0,
	:earth 		=> 0,
	:poison 		=> 0,
	:gravity 		=> 0,
	:water 		=> 0,
	:wind 		=> 0,
	:holy 		=> 0
	}
  end
  
  def default_elems_defense
    @elems_defense	= {
	:fire			=> 0,
	:ice			=> 0,
	:lightning 	=> 0,
	:earth 		=> 0,
	:poison 		=> 0,
	:gravity 		=> 0,
	:water 		=> 0,
	:wind 		=> 0,
	:holy 		=> 0
	}
  end
  
  def check_hp
	if @curr_hp <= 0
	  @curr_hp = 0
	  @status[:death] = true
	  puts "#{@name} is death"
	  elsif @curr_hp > @hp
		@curr_hp = @hp
    end
  end
  
  def check_mp
    if @curr_mp <= 0
      @curr_mp = 0
    elsif @curr_mp > @mp
      @curr_mp = @mp
    end
  end
  
  def behaviour(target)
	@ai.call(target)
  end
end

#ENEMY
#enemy = Enemy.new(name, level, hp, mp, ap, gil, win, att, mat, df, mdf, dfp, dex, lck)
MP = Enemy.new("MP", 2, 30, 0, 10, 2, 10, 0, 6, 0, 4, 0, 0, 50, 4) do |target|
  x = rand (2)
  if x == 0
	puts "Attack"
	Skill::Attack.use(Enemies::MP, target)
	elsif x == 1 
	  puts "Fire"
	  Skill::Fire.use(Enemies::MP, target)
	end
  end
 
Grunt = Enemy.new("Grunt", 7, 40, 0, 10, 2, 15, 0, 12, 2, 10, 2, 4, 58, 8) do |target|
	puts "Attack"
	Skill::Attack.use(Enemies::MP, target)
  end
GuardScorpion = Enemy.new("Guard Scorpion", 12, 800, 0, 200, 10, 100, 0, 30, 15, 40, 300, 0, 60, 1) do |target|
  x = rand (2)
  if x == 0
	puts "Attack"
	Skill::Attack.use(Enemies::MP, target)
	puts target.curr_hp
	elsif x == 1 
	  puts "Fire"
	  Skill::Fire.use(Enemies::MP, target)
	  puts target.curr_hp
	end
  end
	


end